# API FRENCH QURAN - Work In Progress

FRENCH QURAN API is a REST API that allows French speakers to retrieve verses from the noble Quran

## Installation

Use [composer](https://getcomposer.org/) to install API FRENCH QURAN.

```bash
composer install
```

## Importing the verses of the noble Quran

```bash
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
php bin/console doctrine:database:import dump/surah_import.sql
php bin/console doctrine:database:import dump/verse_import.sql
```

## Sources
Source of the digital version of the Quran: [https://quranenc.com/](https://quranenc.com/)

Division in Hizb and Juz, and precision on the place of the revelation: [https://github.com/mehdi-stark/Coran-Quran](https://github.com/mehdi-stark/Coran-Quran)

## Translation
Translation of the meanings of the Noble Quran in French language by Muhammad Hamidullah and published by the King Fahd Complex for the printing of the Noble Quran in Al Madinah Al Munawwarah in the year 1432 of the Hegira. Note: The translation of some verses (which are indicated) have been corrected by the Rawwad Translation Center while allowing access to the initial translation in order to receive suggestions from readers and to continually evaluate and develop our work.

## License
[MIT](https://choosealicense.com/licenses/mit/)