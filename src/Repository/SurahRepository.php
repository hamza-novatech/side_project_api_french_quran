<?php

namespace App\Repository;

use App\Entity\Surah;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Surah|null find($id, $lockMode = null, $lockVersion = null)
 * @method Surah|null findOneBy(array $criteria, array $orderBy = null)
 * @method Surah[]    findAll()
 * @method Surah[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurahRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Surah::class);
    }

    // /**
    //  * @return Surah[] Returns an array of Surah objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Surah
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
