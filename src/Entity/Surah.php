<?php

namespace App\Entity;

use App\Repository\SurahRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SurahRepository::class)
 */
class Surah
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $frenchName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $revelation;

    /**
     * @ORM\OneToMany(targetEntity=Verse::class, mappedBy="surah")
     */
    private $verses;

    public function __construct()
    {
        $this->verses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFrenchName(): ?string
    {
        return $this->frenchName;
    }

    public function setFrenchName(string $frenchName): self
    {
        $this->frenchName = $frenchName;

        return $this;
    }

    public function getRevelation(): ?string
    {
        return $this->revelation;
    }

    public function setRevelation(string $revelation): self
    {
        $this->revelation = $revelation;

        return $this;
    }

    /**
     * @return Collection|Verse[]
     */
    public function getVerses(): Collection
    {
        return $this->verses;
    }

    public function addVerse(Verse $verse): self
    {
        if (!$this->verses->contains($verse)) {
            $this->verses[] = $verse;
            $verse->setSurah($this);
        }

        return $this;
    }

    public function removeVerse(Verse $verse): self
    {
        if ($this->verses->contains($verse)) {
            $this->verses->removeElement($verse);
            // set the owning side to null (unless already changed)
            if ($verse->getSurah() === $this) {
                $verse->setSurah(null);
            }
        }

        return $this;
    }
}
